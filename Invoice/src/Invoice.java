
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class Invoice {
	public static final String FORMAT_STRING = "| %19s | %15s | %14s | %13s | %13s | %10s |";
	public static final String FORMAT_STRING2 = " %70s | %11s | %13s |";
	public static final String FORMAT_STRING3 = " %84s | %13s |";
	public static final String LINE_HEADER = "-------------------------------------------------------------------------------------------------------";
	public static final String LINE_HEADER2="									-------------------------------";
	public static final String LINE_HEADER3="										      -----------------";
	
	public static final String HEADER = LINE_HEADER + "\n" + String.format(FORMAT_STRING, "SALES PERSON",
			"SHIPPING METHOD", "SHIPPING TERMS", "DELIVERY DATE", "PAYMENT TERMS", "DUE DATE") + "\n" + LINE_HEADER
			+ "\n";
	
	private String invoiceNumber;
	private String to;
	private String shipTo = "";
	private Date fecha;
	private String salesPerson;
	private String shipingMethod;
	private String shipingTerms;
	private Date deliveryDate;
	private String paymentTerms;
	private Date dueDate;
	private InvoiceLine[] lines;
	private int totalDiscount;

	

	public Invoice(String invoiceNumber, String to, String shipTo, Date fecha, String salesPerson, String shipingMethod,
			String shipingTerms, Date deliveryDate, String paymentTerms, Date dueDate,int totalDiscount, InvoiceLine... lines) {
		this.lines = lines;
		this.invoiceNumber = invoiceNumber;
		this.shipTo = shipTo;
		this.to = to;
		this.fecha = fecha;
		this.salesPerson = salesPerson;
		this.shipingMethod = shipingMethod;
		this.shipingTerms = shipingTerms;
		this.deliveryDate = deliveryDate;
		this.paymentTerms = paymentTerms;
		this.dueDate = dueDate;
		this.totalDiscount=totalDiscount;//En porcentaje
		calculateInvoiceTotalPrice();

	}

	public String normalDate(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(d);
		return date;
	}
	
	public String normalLargeDate(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM,yyyy");
		String date = sdf.format(d);
		return date;
	}

	public String printInvoiceNumber() {
		return invoiceNumber;
	}

	public String printFecha() {
		return normalLargeDate(fecha);
	}

	public String printTo() {
		return to;

	}

	public String printShipTo() {
		return shipTo;
	}

	public String lineInformation() {

		String s = HEADER;
		s += String.format(FORMAT_STRING, salesPerson, shipingMethod, shipingTerms, normalDate(deliveryDate),
				paymentTerms, normalDate(dueDate)) + "\n" + LINE_HEADER + "\n\n\n" + InvoiceLine.HEADER;

		return s;
	}
	
	public double calculateInvoiceTotalPrice() {
		double totalPrice=0;
		for(int i=0;i<lines.length;i++) {
			totalPrice+=lines[i].calculateLineTotal();
		}
		
		return totalPrice;
		
	}
	
	public double calculateDiscountedPrice() {
		return (calculateInvoiceTotalPrice()*totalDiscount/100);
	}
	
	public double calculateSubtotal() {
		return calculateInvoiceTotalPrice()-calculateDiscountedPrice();
	}
	
	public double calculateVat() {
		return (calculateSubtotal()*21)/100;
	}
	
	public double calculateTotal() {
		return calculateSubtotal()+calculateVat();
	}
	
	public String formatDiscount() {
		String s=String.format(FORMAT_STRING2,"Total Discount",totalDiscount,customFormat("$###,###.##", calculateDiscountedPrice()));
		return s;
	}
	
	public String formatSubtotal() {
		String s=String.format(FORMAT_STRING3,"Subtotal",customFormat("$###,###.##",calculateSubtotal()));
		return s;
	}
	
	public String formatVat() {
		String s=String.format(FORMAT_STRING3,"Vat",customFormat("$###,###.##",calculateVat()));
		return s;
	}
	
	public String formatTotal() {
		String s=String.format(FORMAT_STRING3,"Total",customFormat("$###,###.##",calculateTotal()));
		return s;
	}
	public String customFormat(String pattern, double value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}
	
	
	
	
	@Override
	public String toString() {
		String s="";
		s= printInvoiceNumber() + "\n"+printFecha()+"\n"+printTo()+"\n"+printShipTo()+"\n\n\n\n"+lineInformation()+"\n";
		
		for(int i=0;i<lines.length;i++) {
			s+= lines[i]+"\n";
		}
		s+=LINE_HEADER+"\n";
		s+=formatDiscount()+"\n"+LINE_HEADER2+"\n";
		s+=formatSubtotal()+"\n"+LINE_HEADER3+"\n";
		s+=formatVat()+"\n"+LINE_HEADER3+"\n";
		s+=formatTotal()+"\n"+LINE_HEADER3+"\n";
		return s;
	}
}
