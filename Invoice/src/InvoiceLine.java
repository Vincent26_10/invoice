import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class InvoiceLine {

	private int qty;
	private String item;
	private String description;
	private double unitPrice;
	private double discount;

	public static final String LINE_HEADER = "-------------------------------------------------------------------------------------------------------";
	public static final String FORMAT_STRING = "| %9s | %-10s | %-30s | %11s | %10s%% | %13s |";

	public static final String HEADER = LINE_HEADER + "\n"
			+ String.format(FORMAT_STRING, "QTY", "ITEM", "DESCRIPTION", "UNIT PRICE", "DISCOUNT", "LINE TOTAL") + "\n"
			+ LINE_HEADER;

	public InvoiceLine(int qty, String item, String description, float unitPrice, float discount) {
		this.qty = qty;
		this.item = item;
		this.description = description;
		this.unitPrice = unitPrice;
		this.discount = discount;

	}

	public double calculateLineTotal() {
		double	lineTotal = unitPrice * qty - ((unitPrice * qty) * (discount / 100));
		return lineTotal;
	}
	


	public String customLineFormat(String pattern, double value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}
	
	
	public String toString() {
		String s = String.format(FORMAT_STRING, customLineFormat("###,###", qty), item, description,
				customLineFormat("$###.##", unitPrice), customLineFormat("###.##", discount),
				customLineFormat("$###.##", calculateLineTotal()));
		return s;
	}
}
